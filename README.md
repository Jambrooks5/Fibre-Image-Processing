To whom it may concern, this folder contains all the code written during my MPhys project for analysing anti-resonant hollow core fibres, with a single layer of capillaries with gaps in between. The code is not in a polished state, and there will be left over, unneccessary imports in each program, probably. 

GUIFullProgram.py is the program you need to run, but it requires having other programs from the repository, listed below. 
The key thing is to put the address of your fibre image in the cv2.imread line (33ish). You also need a conversion number of pixels per micron if you want numerical resutls. The two images primarily used in testing of the code were KUV.jpg, for which this value is 29, and Kflat.jpg, for which it is 1.45. 
You will need to install the various python libraries imported at the top of the programs.
After that, the GUI should lead you through the process, explaining the options you can take.
Hint, don't choose "Fancy mask" or "Circle finding", they won't give great results.

Other progams neccessary for running GUIFullProgram:
fromScratch.py (Contains most of the juicy code that takes measurements and does calculations on the fibre)

CallableMasker.py (Open the GUI to let the user select a masking threshold)

manualCenterFinder.py (GUI to allow user to manually select centre of fibre. Don't worry, a more accurate centre will be automatically found by the coder later on)

CallableGUICapFinder2.py (Ignore the one without the "2". This opens a GUI showing currently detected capillary edges, with a slider to adjust detection threshold, and buttons to manually delete false detections)

plotFitting.py (Contains a little code to fit a sine curve to the radius plot, which is then used to recalculate the centre)

OPTIONAL (If not installed, don't select the options for them in the GUI):
callableCircleFinder.py (Uses an OpenCV circle finding function to either select the jacket inner diameter, or the capillaries. It's a bit rubbish though.)

simonsMagic.py (Early version of the code written by my project partner, Simon Cox, in this state it isn't really helpful though, as the resolution it processes the image at isn't enough for accurate measurements)

-----

There are lots of scraps of other programs. The only other useful one could be -
GUIRotateOverlay.py (and the programs it imports.) This uses Simon's later code to find the centre of the fibre through the symmetry of the image, then overlays two images of fibre, centre to centre. It provides a slider to rotate one image ontop of the other, so you can manually assess the symmetry of an image.

